<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_roles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });


        Schema::create('contact_relations', function (Blueprint $table) {
            $table->id();
            //$table->bigInteger('contact_id');
            $table->foreignId('contact_id')
                ->references('id') 
                ->on('contacts')
                ->onDelete('cascade');
            $table->bigInteger('role_id')->nullable();
            $table->foreignId('company_id')
                ->references('id') 
                ->on('contacts')
                ->onDelete('cascade');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->unique(['contact_id','role_id','company_id']);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_roles');
        Schema::dropIfExists('contact_relations');
    }
};
