<?php
 
namespace KDA\Laravel\Contacts\Models\Relations;
 
use Illuminate\Database\Eloquent\Relations\Pivot;
 
class ContactRelation extends Pivot
{
    public $incrementing = true;
    protected $table =  'contact_relations';

    protected $fillable =[
        'phone',
        'email',
        'role_id',
        'contact_id',
        'company_id'
        
    ];
   /* public function role(){
        return $this->belongsTo(Role::class,'role_id');
    }*/
}