<?php

namespace KDA\Laravel\Contacts\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\Contacts\Database\Factories\ContactRoleFactory;

class ContactRole extends Model
{
    use HasFactory;
    protected $fillable= [
        'name'
    ];

    protected static function newFactory()
    {
        return  ContactRoleFactory::new();
    }
}
