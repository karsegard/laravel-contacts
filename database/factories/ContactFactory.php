<?php

namespace KDA\Laravel\Contacts\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\Contacts\Models\Contact;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Contact>
 */
class ContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Contact::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title'=>$this->faker->title(),
            'firstname'=>$this->faker->firstName(),
            'lastname'=>$this->faker->lastName(),
            'address'=>$this->faker->streetAddress(),
            'zip'=>$this->faker->postcode(),
            'city'=>$this->faker->city(),
            'general_phone'=>$this->faker->phoneNumber(),
            'general_email'=>$this->faker->email()
        ];
    }
}
