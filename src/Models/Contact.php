<?php

namespace KDA\Laravel\Contacts\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use KDA\Laravel\Contacts\Database\Factories\ContactFactory;
use KDA\Laravel\Contacts\Models\Relations\ContactRelation;

class Contact extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'title',
        'firstname',
        'lastname',
        'address',
        'zip',
        'city',
        'general_phone',
        'general_email',
        'is_company'
    ];

    protected $casts = [
        'is_company'=>'boolean'
    ];

   /* protected $appends = [
        'full_name'
    ];
*/
    protected static function newFactory()
    {
        return  ContactFactory::new();
    }
    public function getScenarioAttribute()
    {
        return $this->fields_one . " " . $this->fields_two;
    }
    public function companies()
    {
        return $this->belongsToMany(Contact::class,'contact_relations','contact_id','company_id')
        ->withPivot('id','role_id','phone','email')
        ->using(ContactRelation::class)
        ->as('companies');
    }

    public function contacts()
    {
        return $this->belongsToMany(Contact::class,'contact_relations','company_id','contact_id')
        ->withPivot('id','role_id','phone','email')
        ->using(ContactRelation::class)
        ->as('contacts');

    }
/*
    public function roles()
    {
        return $this->belongsToMany(Role::class)->using(ContactRelation::class);
    }*/
}
