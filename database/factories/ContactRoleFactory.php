<?php

namespace KDA\Laravel\Contacts\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\Contacts\Models\ContactRole;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ContactRole>
 */
class ContactRoleFactory extends Factory
{
    protected $model = ContactRole::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            //
            'name'=>$this->faker->jobTitle()
        ];
    }
}
