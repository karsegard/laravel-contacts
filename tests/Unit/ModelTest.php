<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Contacts\Models\Contact;
use KDA\Laravel\Contacts\Models\ContactRole;
use KDA\Tests\TestCase;

class ModelTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function can_create_contact()
  {
    $o = Contact::factory()->create([]);
    $this->assertNotNull($o);
    
  }

 /** @test */
 function can_create_role()
 {
   $o = ContactRole::factory()->create([]);
   $this->assertNotNull($o);
   
 }

  
}